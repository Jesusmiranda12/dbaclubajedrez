create database clubajedrez;
use clubajedrez;
#region creacion de tablas
/*PARTICIPANTES*/
create table participantes(idparticipante int auto_increment PRIMARY KEY, numero_asociado varchar(10), nombre varchar(100), 
apellidop varchar(100), apellidom varchar(100), direccion varchar(150), telefono varchar(10), paisquerepresenta int, 
foreign key(paisquerepresenta) references paises(idpais));
/*JUGADORES*/
create table jugadores(idjugador int primary key, nivel int,
foreign key (idjugador) references participantes(idparticipante));
/*ARBITROS*/
create table arbitros(idarbitro int primary key, foreign key(idarbitro) references participantes(idparticipante));
/*PAISES*/
create table paises(idpais int AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(50), numeroclubs int, fkpaisrepresentante int,
FOREIGN KEY (fkpaisrepresentante) REFERENCES paises(idpais));
/*HOTELES*/
create table hoteles(Idhotel int primary key auto_increment, nombreh varchar(100), direccion varchar(100), 
telefono varchar(100));
/*SALAS*/
create table salas(Idsala int primary key auto_increment, numerosala int, capacidad int, fkhotel int, medios varchar(200),
foreign key(fkhotel) references hoteles(Idhotel));
/*HOSPEDAJES*/
create table hospedajes( id int auto_increment primary key, fkparticipante int, fkhotel int, fechaentrada varchar(50),
fechasalida varchar(50), foreign key (fkparticipante) references participantes(idparticipante),
foreign key (fkhotel) references hoteles(idhotel));
/*PARTIDAS*/
create table partidas( idpartida int auto_increment primary key, jugadorblancas int, jugadornegras int, arbitro int, sala int,
fecha varchar(50), FOREIGN KEY (jugadorblancas) REFERENCES jugadores(idjugador), FOREIGN KEY (jugadornegras) REFERENCES jugadores(idjugador),
FOREIGN KEY (arbitro) REFERENCES arbitros(idarbitro), FOREIGN KEY (sala) REFERENCES salas(idsala));
/*MOVIMIENTOS*/
create table movimientos( idmovimiento int auto_increment primary key, numerodemovimiento int, posiciones varchar(20),
comentario varchar(50), fkpartida int, FOREIGN KEY (fkpartida) REFERENCES partidas(idpartida));
#end region
#region procedures
#region Hoteles
create procedure p_insertarHoteles(in _id int, in _nombreh varchar(101), in _direccion varchar(100), in _telefono varchar(10))
begin 
insert into hoteles values(null, _nombreh , _direccion, _telefono);
end;
create procedure p_eliminarHoteles(in _id int)
begin
delete from Hoteles where Idhotel = _id;
end;
create procedure p_actualizarHoteles(in _nombreh varchar(100), in _direccion varchar(100), in _telefono varchar(10), in _id int)
begin
update hoteles set nombreh = _nombreh, direccion = _direccion, telefono = _telefono where Idhotel = _id;
end;
#end region
#region Paises
create procedure p_insertarPaises(in _id int, in _nombre varchar(50), in _numeroclubs int, in _fkpaisrepresentante int)
begin 
insert into paises values(null, _nombre , _numeroclubs, _fkpaisrepresentante);
end;
create procedure p_eliminarPaises(in _id int)
begin
delete from paises where idpais = _id;
end;
create procedure p_actualizarPaises(in _nombre varchar(50), in _numeroclubs int, in _fkpaisrepresentante int, in _id int)
begin
update paises set nombre = _nombre, numeroclubs = _numeroclubs, fkpaisrepresentante = _fkpaisrepresentante where idpais = _id;
end;
#end region
#region Salas
create procedure p_insertarSalas(in _id int,in _numerosala int, in _capacidad int, in _fkhotel int, in _medios varchar(200))
begin 
insert into salas values(null, _numerosala , _capacidad, _fkhotel, _medios);
end;
create procedure p_eliminarSalas(in _id int)
begin
delete from salas where Idsala = _id;
end;
create procedure p_actualizarSalas(in _numerosala varchar(100), in _capacidad varchar(100),in _fkhotel int,
in medios varchar(200),in _id int)
begin
update salas set numerosala = _numerosala, capacidad = _capacidad, fkhotel = _fkhotel, medios = _medios
where Idsala = _id;
end;
#end region
#region movimientos
create procedure p_insertarMovimientos(in _id int, in _numerodemovimiento int, in _posiciones varchar(20), in _comentario varchar(50),
in _fkpartida int)
begin 
insert into movimientos values(null, _numerodemovimiento, _posiciones, _comentario, _fkpartida);
end;
create procedure p_eliminarMovimientos(in _id int)
begin
delete from movimientos where idmovimiento = _id;
end;
create procedure p_actualizarMovimientos(in _numerodemovimiento int, in _posiciones varchar(20), in _comentario varchar(50),
in _fkpartida int, in _id int)
begin
update movimientos set numerodemovimiento = _numerodemovimiento, posiciones = _posiciones, cometario = _comentario, 
fkpartida = _fkpartida where idmovimiento = _id;
end;
#end region
#region participantes
create procedure p_insertarParticipantes(in _id int,in _numero_asociado varchar(10), in _nombre varchar(100), _apellidop varchar(100),
_apellidom varchar(100), _direccion varchar(150), _telefono varchar(10), in _pais int)
begin
insert into participantes values(_id,_numero_asociado,_nombre,_apellidop,_apellidom,_direccion,_telefono,_pais);
end;
create procedure p_eliminarParticipante(in _id int)
begin
delete from participantes where idparticipante = _id;
end;
create PROCEDURE p_actualizarParticipante(in _numero_asociado varchar(10), in _nombre varchar(100), _apellidop varchar(100),
_apellidom varchar(100), _direccion varchar(150), _telefono varchar(10), in _pais int,in id int)
begin
update participantes set numero_asociado = _numero_asociado,nombre = _nombre,apellidop=_apellidop,apellidom=_apellidom,
direccion = _direccion,telefono = _telefono, paisquerepresenta=_pais where idparticipante = id;
end;
#end region
#region jugadores
create PROCEDURE p_insertarJugadores(in _idjugador int,in _nivel int)
begin
insert into jugadores values(_idjugador,_nivel);
end;
create procedure p_eliminarJugador(in _idjugador int)
begin
delete from jugadores where idjugador=_idjugador;
end;
create PROCEDURE p_actualizarJugadores(in _nivel int,in _idjugador int)
begin
update jugadores set nivel = _nivel where idjugador = _idjugador;
end;
#end region
#region arbitros
create procedure p_insertarArbitros(in _idarbitro int)
begin
insert into arbitros values(_idarbitro);
end;
create procedure p_eliminarArbitro(in _idarbitro int)
begin
delete from arbitros where idarbitro=_idarbitro;
end;
create procedure p_actualizarArbitro(in _idarbitro int,in _id int)
begin
update arbitros set idarbitro = _idarbitro where idarbitro= _id;
end;
#end region
#region hospedajes
create procedure p_insertarHospedajes(in _id int, in _participante int, in _hotel int, in _fechaentrada varchar(50),
in _fechasalida varchar(50))
begin
insert into hospedajes values(_id,_participante,_hotel, _fechaentrada,_fechasalida);
end;
create procedure p_eliminarHospedaje(in _id int)
begin
delete from hospedajes where id = _id;
end;
create procedure p_actualizarHospedajes(in _participante int, in _hotel int, in _fechaentrada varchar(50),
in _fechasalida varchar(50),in _id int)
begin
update hospedajes set fkparticipante= _participante,fkhotel=_hotel,fechaentrada =_fechaentrada,fechasalida = _fechasalida
where id=_id;
end;
#end region
#region procedures para partidas
create procedure p_insertarPartidas(in _idpartida int,in _jugadorblancas int,in _jugadornegras int, in _arbitro int,
in _sala int, in _fecha varchar(50))
begin
insert into partidas values(_idpartida,_jugadorblancas,_jugadornegras,_arbitro,_sala,_fecha);
end;
create procedure p_eliminarPartidas(in _id int)
begin
delete from partidas where idpartida = _id;
end;
create procedure p_actualizarPartidas(in _jugadorblancas int,in _jugadornegras int, in _arbitro int,
in _sala int, in _fecha varchar(50),in _id int)
begin
update partidas set jugadorblancas=_jugadorblancas,jugadornegras=_jugadornegras,arbitro=_arbitro,sala=_sala,fecha=_fecha
where idpartida=_id ;
end;
#end region
#end region
